import java.util.Scanner;
import static java.lang.Character.toUpperCase;
/**
 * @author Alejandro Villarreal
 *
 */

public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        Board game = new Board();
        game.initializeBoard();

            /**
             * Setting up I tetronimo
             */
            game.setPlay(19, 0, "I");
            game.setPlay(19, 1, "I");
            game.setPlay(19, 2, "I");
            game.setPlay(19, 3, "I");

            /**
             * Setting up T tetronimo
             */
            game.setPlay(18, 6, "T");
            game.setPlay(19, 5, "T");
            game.setPlay(19, 6, "T");
            game.setPlay(19, 7, "T");

            /**
             * Setting up Z tetronimo
             */
            game.setPlay(18, 7, "Z");
            game.setPlay(18, 8, "Z");
            game.setPlay(19, 8, "Z");
            game.setPlay(19, 9, "Z");
            System.out.println(game.printBoard());


        int random = (int)(Math.random() * 7 + 1);
        String tetrisPiece = "";

        if (random == 1){

            game.setPlay(4, 5, "O");
            game.setPlay(5, 4, "O");
            game.setPlay(4, 4, "O");
            game.setPlay(5, 5, "O");
            tetrisPiece = "O";
        }
        if (random == 2){

            game.setPlay(4, 3, "I");
            game.setPlay(4, 4, "I");
            game.setPlay(4, 5, "I");
            game.setPlay(4, 6, "I");
            tetrisPiece = "I";
        }
        if (random == 3){

            game.setPlay(4, 3, "T");
            game.setPlay(4, 4, "T");
            game.setPlay(4, 5, "T");
            game.setPlay(5, 4, "T");
            tetrisPiece = "T";
        }
        if (random == 4){

            game.setPlay(4, 3, "Z");
            game.setPlay(4, 4, "Z");
            game.setPlay(5, 4, "Z");
            game.setPlay(5, 5, "Z");
            tetrisPiece = "Z";
        }
        if (random == 5){

            game.setPlay(5, 3, "S");
            game.setPlay(5, 4, "S");
            game.setPlay(4, 4, "S");
            game.setPlay(4, 5, "S");
            tetrisPiece = "S";
        }
        if (random == 6){

            game.setPlay(4, 5, "J");
            game.setPlay(5, 5, "J");
            game.setPlay(6, 5, "J");
            game.setPlay(6, 4, "J");
            tetrisPiece = "J";
        }
        if (random == 7){

            game.setPlay(4, 4, "L");
            game.setPlay(5, 4, "L");
            game.setPlay(6, 4, "L");
            game.setPlay(6, 5, "L");
            tetrisPiece = "L";
        }

        System.out.println(game.printBoard()+ "\nDisplaying tetris piece " + tetrisPiece);

        System.out.println("\nWould you like to move Tetris Piece ("+ tetrisPiece+ ") Left(L), or Right(R)\nor Rotate Clockwise (A), Rotate CounterClockwise(Z), or Quit(Q): ");
        char option = input.next().charAt(0);
        char accepted = toUpperCase(option);

        do {
            if(accepted == 'A'){
                //Tetromino.rotateLeft();
                //Tetromino.rotateLeft();
                //Tetromino.rotateLeft();
                //Tetromino.rotateLeft();

            }

            if(accepted == 'Z'){
                //Tetromino.rotateRight();
                //Tetromino.rotateRight();
                //Tetromino.rotateRight();
                //Tetromino.rotateRight();

            }

            if(accepted == 'L'){
                game.moveTetrominoLeft();
                game.moveTetrominoLeft();
                game.moveTetrominoLeft();
                game.moveTetrominoLeft();

            }

            if(accepted == 'R'){
                game.moveTetrominoRight();
                game.moveTetrominoRight();
                game.moveTetrominoRight();
                game.moveTetrominoRight();

            }

            if(accepted == 'Q'){
                System.out.println("Goodbye");
                break;
            }

            System.out.println("\nThat is invalid please select either Left(L), Right(R),\nRotate Clockwise (A), Rotate CounterClockwise(Z), or Quit(Q): ");
            option = input.next().charAt(0);
            accepted = toUpperCase(option);

        }while(accepted != 'Q');

    }
}