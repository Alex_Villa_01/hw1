import java.awt.*;
import javax.swing.*;

/**
 * Model class from MVC. This class contains the Tretris logic but no GUI information. 
 * Model must not know anything about the GUI and Controller.
 * @author Alejandro Villarreal
 *
 */
public class Board extends java.util.Observable{
	
	private String[][] board;
    private static final int ROWS = 20;
    private static final int COLS = 10;
    private String identifier = "\\s{3}";
	private int level;
	private int score;
	private boolean isGameActive;


	public Board()	{
        board = new String[ROWS][COLS]; 
	}
	
	 /**
     * Initialize board
     */

    public void initializeBoard(){
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS; j++){
                board[i][j] = "   ";
            }
       }
    }

    public void setPlay(int i, int j, String pieceIdentifier){
        if (this.board[i][j].matches(identifier))
            board[i][j] = " "+ pieceIdentifier + " ";
    }

    /**
     * prints tetris board
     * @return strboard
     */

    public String printBoard(){
        String strBoard = "";
        strBoard += "  1   2   3   4   5   6   7   8   9   10";
        strBoard += "\n+---+---+---+---+---+---+---+---+---+---+\n";
        int n = 1;
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS; j++){
                strBoard += "|" + board[i][j];
            }
            strBoard += "|" + n;
            n=n+1;
            strBoard += "\n+---+---+---+---+---+---+---+---+---+---+\n";

        }
        return strBoard;
    }

	/**
	 * Returns if game is active
	 * @return
	 */
	public boolean isGameActive()
	{
		return isGameActive;
	}
	/**
	 * Returns score
	 * @return
	 */
	public int getScore()
	{
		return score;
	}
	/**
	 * Return current level
	 * @return
	 */
	public int getLevel()
	{
		return level;
	}
	/**
	 * Moves tetromino down
	 */
	public void moveTetrominoDown()
	{
		//your code goes here
	}
	public void moveTetrominoRight()
	{
		//your code goes here
	}
	public void moveTetrominoLeft()
	{
		//your code goes here
	}
	/**
	 * Returns if this is a valid position
	 * @return
	 */
	private boolean validateTetrominoPosition()
	{
		//your code goes here
		return true;
	}
}